import { Button, Col, Row } from 'react-bootstrap';

export default function Banner() {
	return (
		
		<Row>
			<Col className="p-5">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everyone, everywhere.</p>
				<Button variant="primary">Enroll Now!</Button>
			</Col>
		</Row>
	)
}

/*
- The "className" prop is used in place of the "class" attribute for HTML tags in React JS due to our use of JSX elements.
*/
