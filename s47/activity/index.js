const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");


txtFirstName.addEventListener("keyup", FullNamePrint, (event) => {

				spanFullName.innerHTML = txtFirstName.value
	})

// Alternative way to write the code for event handling
txtLastName.addEventListener("keyup", FullNamePrint);

 function printLastName(event){

 	spanFullName.innerHTML = txtLastName.value;
 }

txtLastName.addEventListener("keyup", (event) =>{
		console.log(event);
		console.log(event.target);
		console.log(event.target.value);
});

function FullNamePrint(event) {
	
	const firstName = txtFirstName.value;
	const lastName = txtLastName.value;

	spanFullName.innerHTML = firstName + " " + lastName;
};
