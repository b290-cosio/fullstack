// console.log("Meron pang time")

// mock data = will contain all the posts
let posts = [];
// will be the id
let count = 1;

// CREATE(add post)
document.querySelector("#form-add-post").addEventListener("submit", e =>{

	e.preventDefault();

	posts.push(
	{
		id : count,
		title : document.querySelector("#txt-title").value,
		body : document.querySelector("#txt-body").value
	});

	// next available id(incremental value for id)

	count++;

	showPosts(posts);
	alert("Successfully added!")
})

const showPosts = posts =>{

	//Create a variable that will contain all the posts
	let postEntries = "";

	posts.forEach(post =>{

		postEntries += `
	
	<div id="post-${post.id}">
	  <h3 id="post-title-${post.id}">${post.title}</h3>
	  <p id="post-body-${post.id}">${post.body}</p>
	  <button onclick="editPost('${post.id}')">Edit</button>
	  <button onclick="deletePost('${post.id}')">Delete</button>
	</div>


		`
	})

	// To check what is stored in the postEntries variables.
	console.log(postEntries);

	// To assign the value of postEntries to the element with "div-post-entries" id.
	document.querySelector("#div-post-entries").innerHTML = postEntries;

}

// edit button function
const editPost = id => {
  const title = document.querySelector(`#post-title-${id}`).innerHTML;
  const body = document.querySelector(`#post-body-${id}`).innerHTML;

  document.querySelector("#txt-edit-id").value = id;
  document.querySelector("#txt-edit-title").value = title;
  document.querySelector("#txt-edit-body").value = body;

  document.querySelector("#btn-submit-update").removeAttribute("disabled");
};

// UPDATE (update post)
document.querySelector("#form-edit-post").addEventListener("submit", e => {
  e.preventDefault();

  // loop over the array to check for the id that is the same with the element's id and text edit field
  for (let i = 0; i < posts.length; i++) {

    // The value posts[i].id is a Number while document.querySelector('#txt-edit-id').value is a String.
    // Therefore, it is necessary to convert the Number to a String first.
    if (posts[i].id.toString() === document.querySelector("#txt-edit-id").value) {

      // re-assign the property values 
      posts[i].title = document.querySelector("#txt-edit-title").value;
      posts[i].body = document.querySelector("#txt-edit-body").value;

      showPosts(posts);
      alert("Successfully updated!");

      document.querySelector("#txt-edit-title").value = null;
      document.querySelector("#txt-edit-body").value = null;

      // Resetting disabled attribute for button
      document.querySelector("#btn-submit-update").setAttribute("disabled", true);

      // stop the loop once changes are made
      break;
    }
  }
});

// ------- READING LIST -------

const deletePost = id => {
  const index = posts.findIndex(post => post.id === id);
  if (index !== -1) {
    posts.splice(index, 1);
  }
  const postElement = document.querySelector(`#post-${id}`);
  if (postElement) {
    postElement.remove();
  }
};